﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaAttributes : ScriptableObject
{
	public int MaxHealth = 100;
	public int Lives = 3;
	
	public NinjaAttributes Clone() {
		var newAttrs = CreateInstance<NinjaAttributes>();

		newAttrs.MaxHealth = MaxHealth;
		newAttrs.Lives = Lives;
		
		return newAttrs;
	}	
}
