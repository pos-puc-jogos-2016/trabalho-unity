﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimEventHandler : MonoBehaviour
{
	public enum EventType { intType, boolType, triggerType, spawnParticle }
	public List<AnimEventType> AnimEvents;

	public void AnimEvent(string eventName)
	{
		foreach (var animEvent in AnimEvents)
		{
			if (animEvent.Type == EventType.triggerType)
				gameObject.Send<IAnimController>(_ => _.SetTrigger(animEvent.name));
			else if (animEvent.Type == EventType.spawnParticle)
				gameObject.Send<IAnimController>(_ => _.SpawnParticle(Game.Data.Particles.HitEffect));
		}
	}
	
	[System.Serializable]
	public class AnimEventType
	{
		public string name;
		public EventType Type;
	}
}

