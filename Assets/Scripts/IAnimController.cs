﻿
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public interface IAnimController : IEventSystemHandler {
	IEnumerable SetTrigger(string triggerName);
	IEnumerable SpawnParticle(Transform particle);
}
