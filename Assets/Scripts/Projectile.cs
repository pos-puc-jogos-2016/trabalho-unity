﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SphereCollider))]
public class Projectile : MonoBehaviour {

    public float projectileSpeed;
    public float projectileTime;
	public int Damage;

	// Update is called once per frame
	void Update () {
		Debug.Log("Fireball Moving");
        transform.position += transform.forward * projectileSpeed;
	}

    void OnCollisionEnter(Collision col)
    {
	    col.gameObject.Send<IArmor>(_ => _.ApplyDamage(Damage));
//        var enemy = col.gameObject.GetComponent<CharacterAttributes>();
//        int result = enemy.doDamage(owner.damage);
//        owner.addExp(result);
		Instantiate(Game.Data.Particles.Explosion, col.contacts[0].point, Quaternion.identity);
//        NPCAI ai = col.gameObject.GetComponent<NPCAI>();
//        if (result == 0 && ai) ai.pursuitTarget(owner.gameObject.transform);
        Destroy(gameObject);
    }
}
