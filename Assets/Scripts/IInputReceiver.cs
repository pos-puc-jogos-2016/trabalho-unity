﻿
using System.Collections;
using UnityEngine.EventSystems;

public interface IInputReceiver : IEventSystemHandler {
	IEnumerable MouseClick(int button);
	IEnumerable FireballPressed();
}
