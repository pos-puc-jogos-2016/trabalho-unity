﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private GameObject _ninjaGo;

    void Start()
    {
        _ninjaGo = Game.Data.Player.gameObject;
    }

    void FixedUpdate()
    {
        //if (Input.GetButton("Jump"))
        if (Input.GetMouseButtonDown(0)) {
            ClickTest();
        }
        
        if (Input.GetMouseButtonDown(1)) {
            _ninjaGo.Send<IAnimController>(_ => _.SetTrigger("attacknow"));
        }

        if (Input.GetButtonDown("Fire3")) {
            ShootFireball();
        }
    }

    private void ShootFireball()
    {
        _ninjaGo.Send<IInputReceiver>(_ => _.FireballPressed());
    }

    void ClickTest()
    {
        var clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        //LayerMask mask = ~LayerMask.NameToLayer("ground"); //1 << 8

        if (Game.Data.GroundCollider.Raycast(clickRay, out hit, Mathf.Infinity)) {
            //if (Physics.Raycast(clickRay, out hit, Mathf.Infinity, mask)){
            Debug.Log("Click worked!");
            Debug.DrawRay(transform.position, clickRay.direction * hit.distance, Color.yellow);
            var ninjaController = Game.Data.Player.GetComponent<NinjaController>();
            _ninjaGo.Send<IInputReceiver>(_ => _.MouseClick(0));
            _ninjaGo.Send<ILocomotor>(_ => _.MoveTo(hit.point));
        }
    }
}