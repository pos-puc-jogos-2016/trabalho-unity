﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NinjaController : MonoBehaviour, IInputReceiver, ILocomotor, IAnimController
{
    private const string IsWalkingStr = "iswalking";
    private Animator _animator;
    private NavMeshAgent _agent;
    public float ReloadTime = 1f;
    public bool _reloading;
    public bool _isWalking;
    public Transform SwordHitpoint;
    public GameObject FireballPrefab;
    private float _originalSpeed;

    public bool HasFireball = false;

    public NinjaAttributes Attributes;
    //public Vector3 Target;

    void Awake()
    {
        //_attributes = 
        _animator = GetComponent<Animator>();
        _agent = GetComponent<NavMeshAgent>();
        Attributes = Resources.Load<NinjaAttributes>("NinjaAttributes");
        Attributes = Attributes.Clone();

        _originalSpeed = _agent.speed;
    }

    void Update()
    {
//		if (!_isWalking)
//			return;

        if (_agent.remainingDistance < _agent.stoppingDistance)
            DoWalk(false);
    }

    void DoWalk(bool state)
    {
        _animator.SetBool(IsWalkingStr, state);
        _isWalking = state;
    }

    public IEnumerable MouseClick(int button)
    {
        if (button == 0)
            Debug.Log("Left mouse clicked");
        yield return null;
    }

    public IEnumerable FireballPressed()
    {
        Debug.Log("Trying to Shoot Fireball");
        if (HasFireball) {
            Instantiate(FireballPrefab, transform.position + (transform.forward * 1) + new Vector3(0, 0.5f, 0),
                transform.rotation);
            HasFireball = false;
        }
        yield return null;
    }

    public IEnumerable MoveTo(Vector3 target)
    {
        _agent.SetDestination(target);
        DoWalk(true);

        yield return null;
    }

    public IEnumerable SetTrigger(string triggerName)
    {
        Debug.Log(triggerName);
        _animator.SetTrigger(triggerName);
        yield return null;
    }

    public IEnumerable SpawnParticle(Transform particle)
    {
        Debug.Log("spawn particle");
        Transform hitEffect = Instantiate(particle, SwordHitpoint);
        yield return null;
    }

    public IEnumerable ChangeSpeed(float percentage, float duration)
    {
        var newSpeed = _agent.speed + (_originalSpeed * percentage);
        Debug.Log("Changing Speed to " + newSpeed);
        _agent.speed = newSpeed;
        yield return new WaitForSeconds(duration);
        Debug.Log("Changing back to Original Speed");
        _agent.speed = _originalSpeed;
        yield return null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("HealthBox")) {
            Game.Data.Player.gameObject.Send<IArmor>(_ => _.RestoreHealth(0.2f));
            Destroy(other.gameObject);
        } else if (other.gameObject.CompareTag("SpeedBox")) {
            Game.Data.Player.gameObject.Send<ILocomotor>(_ => _.ChangeSpeed(0.2f, 20.0f));
            Destroy(other.gameObject);
        } else if (other.gameObject.CompareTag("FireballBox")) {
            HasFireball = true;
            Destroy(other.gameObject);
        }
    }
}