﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour {

    public static Game Data { get; private set; }
    public Collider GroundCollider;

    public Transform Player;
    public ParticleTrans Particles;

    void Awake()
    {
        if (Data == null)
            Data = this;
    }

    void OnDestroy()
    {
        Data = null;
    }

    [System.Serializable]
    public class ParticleTrans
    {
        public Transform HitEffect, FireBall, Explosion;
    }
    
}
