﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
public class NPCAI : MonoBehaviour
{
    public List<Transform> PatrolRoute;

    private int _currentPoint;
    private NavMeshAgent _agent;
    private bool _goingNext = true, _playerAttracted;
    public Animator Animator;
    public Collider[] Colliders;
    public float Radius = 4.0f;
    public LayerMask Mask;
    public float AttackRadius = 0.0f;
    public int Damage;

    private float _attackRate = 2f;
    private float _nextAttack = 0;
    private float _nextFollow = 0;
    private float _followRate = 3f;
    private bool _attackReady = true;

    private void Awake() {
        Animator = GetComponent<Animator>();
    }

    // Use this for initialization
    private void Start() {
        _currentPoint = 0;
        _agent = GetComponent<NavMeshAgent>();
        GoToNextPoint();
    }

    // Update is called once per frame
    private void Update() {
        if (Time.time > _nextAttack) {
            _nextAttack += _attackRate;
            _attackReady = true;
        }

        if (IsCloseToPlayer()) {
            AttackPlayer();
            return;
        }

        Animator.SetBool("iswalking", true);

        if (Time.time > _nextFollow) {
            Debug.Log(Time.time + " Check Follow");
            _playerAttracted = IsAttractedByPlayer();
            _nextFollow += _followRate;
        }
        
        if (_playerAttracted) {
            GoToPlayer();
            return;
        }


        if (IsHitPoint()) {
            UpdateRouteIndex();
        }

        GoToNextPoint();
    }

    private void AttackPlayer() {
        if (_attackReady) {
            Animator.SetBool("iswalking", false);
            Animator.SetBool("attacknow", true);

            Game.Data.Player.gameObject.Send<IArmor>(_ => _.ApplyDamage(Damage));
            _attackReady = false;
        }
    }

    private bool IsAttractedByPlayer() {
        Colliders = Physics.OverlapSphere(transform.position, Radius, Mask);

        return Colliders.Any(collider => collider.gameObject.CompareTag("PlayerNinja"));
    }

    private bool IsHitPoint() {
        return Vector3.Distance(PatrolRoute[_currentPoint].position, transform.position) < 1f;
    }

    private void UpdateRouteIndex() {
        if (_goingNext) {
            if (_currentPoint < PatrolRoute.Count - 1) {
                _currentPoint++;
            }
            else {
                _goingNext = false;
                _currentPoint--;
            }
        }
        else {
            if (_currentPoint > 0) {
                _currentPoint--;
            }
            else {
                _goingNext = true;
                _currentPoint++;
            }
        }
    }

    private void GoToNextPoint() {
        _agent.SetDestination(PatrolRoute[_currentPoint].position);
    }

    private void GoToPlayer() {
        _agent.SetDestination(Game.Data.Player.position);
    }

    private bool IsCloseToPlayer() {
        var attackColliders = Physics.OverlapSphere(transform.position, AttackRadius, Mask);

        return attackColliders.Any(collider => collider.gameObject.CompareTag("PlayerNinja"));
    }
}