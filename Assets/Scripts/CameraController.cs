﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
	public Transform Target;
	public float SmoothFactor;
	public float RotationSmoothing = 1f;

	void Update()
	{
		transform.position = Vector3.Lerp(transform.position,
			Target.position, SmoothFactor * Time.deltaTime);
		var lookAtQuat = Quaternion.LookRotation(
			(Game.Data.Player.position + (Vector3.up * 1.5f))
			- transform.position
		);
		transform.rotation = Quaternion.Slerp(transform.rotation
			, lookAtQuat, RotationSmoothing*Time.deltaTime);
	}
}
