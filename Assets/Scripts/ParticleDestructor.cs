﻿using UnityEngine;
using System.Collections;

public class ParticleDestructor : MonoBehaviour {

    public float timeToDestroy;

	// Use this for initialization
	void Start () {
        StartCoroutine(destroyInTime());
	}

    IEnumerator destroyInTime()
    {
        yield return new WaitForSeconds(timeToDestroy);
        Destroy(gameObject);
    }


}
