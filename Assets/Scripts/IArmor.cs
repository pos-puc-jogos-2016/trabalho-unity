﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public interface IArmor : IEventSystemHandler {
	IEnumerable ApplyDamage(int damage);
	IEnumerable ApplyDamage(int damage, Vector3 pos);
	IEnumerable RestoreHealth(float modifier);
	int? GetHealth();	// request
}
