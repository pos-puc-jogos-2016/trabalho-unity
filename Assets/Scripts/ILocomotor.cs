﻿
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public interface ILocomotor : IEventSystemHandler {
	IEnumerable MoveTo(Vector3 target);
	IEnumerable ChangeSpeed(float speed, float duration);
}
